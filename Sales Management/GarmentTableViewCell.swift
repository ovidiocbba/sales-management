//
//  GarmentTableViewCell.swift
//  Sales Management
//
//  Created by Simon Miranda Chiri on 8/15/16.
//  Copyright © 2016 Miranda. All rights reserved.
//

import UIKit

class GarmentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var salePriceLabel: UILabel!
    @IBOutlet weak var costPriceLabel: UILabel!
    @IBOutlet weak var nameGarmentLabel: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
