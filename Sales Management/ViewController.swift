//
//  ViewController.swift
//  Sales Management
//
//  Created by Simon Miranda Chiri on 8/9/16.
//  Copyright © 2016 Miranda. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool) {
   
        //Aquie se verifica que un usuario este logueado
        let isUserLoggedIN = NSUserDefaults.standardUserDefaults().boolForKey("isUserLoggedIn")
        if(!isUserLoggedIN)
        {
        self.performSegueWithIdentifier("loginView", sender: self)
        }
 
    }
    
    @IBAction func logoutButtonTapped(sender: AnyObject) {
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isUserLoggedIn");
        NSUserDefaults.standardUserDefaults().synchronize();
        //self.performSegueWithIdentifier("loginView", sender: self);
        
        let myViewController:ViewController = self.storyboard!.instantiateViewControllerWithIdentifier("Protected") as! ViewController
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        appDelegate.window?.rootViewController=myViewController
        
        appDelegate.window?.makeKeyAndVisible()
        
      }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "loginView")
        {
            print("Words")
        }
    }
    
    
    
}

