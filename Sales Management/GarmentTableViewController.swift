//
//  GarmentTableViewController.swift
//  Sales Management
//
//  Created by Simon Miranda Chiri on 8/15/16.
//  Copyright © 2016 Miranda. All rights reserved.
//

import UIKit
import RealmSwift

class GarmentTableViewController: UITableViewController {
    
    var garmets = [Garment]()
    
    //var meals = [Meal]()
    var realm:Realm?
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        realm = try! Realm()
        //print (" the path real is \(realm?.path)")
        //print (" the path real is \(Realm.Configuration.defaultConfiguration.fileURL)")
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Use the edit button item provided by the table view controller.
        navigationItem.leftBarButtonItem = editButtonItem()
        loadSampleGarment();
        
        
                              
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    func loadSampleGarment()
    {
        /*
        let  garmet1 = Garment(nameGarment: "Clasic Casual",costPrice: 75,salePrice: 95, description: "Moda 2016",photo:  UIImage(named: "Image"))!;
        let  garmet2 = Garment(nameGarment: "Juvenik Casual",costPrice: 55,salePrice: 75, description: "Moda Juvenil",photo:  UIImage(named: "Image"))!;
        garmets+=[garmet1,garmet2];
       */
        for date in (realm?.objects(GarmentDB.self))! {
            let img = UIImage(data: date.photo!)
            let garment = Garment(nameGarment: date.nameGarment,costPrice: date.costPrice,salePrice: date.salePrice,description:date.descriptionGarment,  photo: img)
            garmets.append(garment!)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return garmets.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        print("Iterating")
        let cell = tableView.dequeueReusableCellWithIdentifier("GarmentTableViewCell", forIndexPath: indexPath) as! GarmentTableViewCell
        let garment = garmets[indexPath.row]

        cell.nameGarmentLabel.text = garment.nameGarment
        cell.costPriceLabel.text = String(garment.costPrice)
        cell.salePriceLabel.text = String(garment.salePrice)
        cell.descriptionLabel.text = garment.description
        cell.photoImageView.image = garment.photo
 
        // Configure the cell...

        return cell
    }
 
    @IBAction func unwindToMealList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.sourceViewController as? RegisterGarmentViewController, garment = sourceViewController.garment {
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                // Update an existing meal.
                garmets[selectedIndexPath.row] = garment
                tableView.reloadRowsAtIndexPaths([selectedIndexPath], withRowAnimation: .None)
            }
            
            else{
            // Add a new garment item.
            let newIndexPath = NSIndexPath(forRow: garmets.count, inSection: 0)
            garmets.append(garment)
            tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Bottom)
            }
 
        }
        
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        /*
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        } 
        */
        if editingStyle == .Delete {
            // Delete the row from the data source
            garmets.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowDetail" {
            print("ShowDetail Garment")
            let garmentDetailViewController = segue.destinationViewController as! RegisterGarmentViewController
            
            // Get the cell that generated this segue.
            if let selectedGarmentCell = sender as? GarmentTableViewCell {
                let indexPath = tableView.indexPathForCell(selectedGarmentCell)!
                let selectedGarment = garmets[indexPath.row]
                garmentDetailViewController.garment = selectedGarment
            }
        }
        else if segue.identifier == "AddItem" {
            print("Adding new Garment.")
        }
    }
    

}
