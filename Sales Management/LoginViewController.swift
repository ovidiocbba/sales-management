//
//  LoginViewController.swift
//  Sales Management
//
//  Created by Simon Miranda Chiri on 8/9/16.
//  Copyright © 2016 Miranda. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var userEmailTextField: UITextField!
    @IBOutlet weak var userPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginButtonTapped(sender: AnyObject) {
        let userEmail = userEmailTextField.text!;
        let userPassword = userPasswordTextField.text!;
        
        if(userPassword.isEmpty || userPassword.isEmpty)
        {
            return;
        }
        //Send user data to server side
        let myUrl = NSURL(string:"http://simonmiranda.esy.es/userLogin.php")
        let request = NSMutableURLRequest(URL:myUrl!);
        request.HTTPMethod = "POST";
        
        let postString = "email=\(userEmail)&&password=\(userPassword)";
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding);
        let task =  NSURLSession.sharedSession().dataTaskWithRequest(request){
            data,response, error in
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            print("response = \(response)")
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString!)")
            
            do {
                
                
                let parseJSON = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String: AnyObject]
                // use anyObj here
                var resultValue = parseJSON["status"] as? String;
                let messageValue = parseJSON["message"] as? String;
                
                print("result:\(resultValue)")
                //Borrar cuando estes con Internet
                 //resultValue = "Success"
                ///////////////
                
                if(resultValue == "Success")
                {
                    //Login is successfull
                    NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isUserLoggedIn");
                    //isUserLoggedIn
                    print("isUserLoggedIn");
                    
                    NSUserDefaults.standardUserDefaults().synchronize();
                    
                    self.dismissViewControllerAnimated(true, completion: nil);
                }
                else
                {
                    print("Error:\(messageValue)")
                    /*
                    dispatch_async(dispatch_get_main_queue(), {
                        //Display alert message with confirmation
                        let myAlert = UIAlertController(title:"Message",message:messageValue, preferredStyle: UIAlertControllerStyle.Alert);
                        
                        let okAction = UIAlertAction(title:"Ok",style: UIAlertActionStyle.Default, handler: nil);
                        
                        myAlert.addAction(okAction);
                        
                        self.presentViewController(myAlert, animated: true, completion: nil)
                    });
                      */
                    
 
                    //Display alert message with confirmation
                    
                }
            
            } catch let error as NSError {
                print("json error: \(error.localizedDescription)")
            }
            
            
            
            
        }
        task.resume();
        
        /*
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        {data,response, error in
            
            if error != nil{
                print("error=\(error)");
                return
            }
        }
        */
        
       
        //var err: NSError?
       // var json = NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers) as? NSDictionary
        
 
        
        
        
        
        
        
        
        
        
        /*Para Guardar de forma local*/
        /*
        let userEmailStored = NSUserDefaults.standardUserDefaults().stringForKey("userEmail");
        
        let userPasswordStored = NSUserDefaults.standardUserDefaults().stringForKey("userPassword");
 
        
        if(userEmailStored == userEmail)
        {
            if(userPasswordStored == userPassword)
            {
                //Login is successfull
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isUserLoggedIn");
                //isUserLoggedIn
                print("isUserLoggedIn");
                NSUserDefaults.standardUserDefaults().synchronize();
                
                self.dismissViewControllerAnimated(true, completion: nil);

            }
            else
            {
                var myAlert = UIAlertController(title:"Alert",message:"Error", preferredStyle: UIAlertControllerStyle.Alert);
                
                let okAction = UIAlertAction(title:"Ok",style: UIAlertActionStyle.Default, handler: nil);
                
                myAlert.addAction(okAction);
                
                self.presentViewController(myAlert, animated: true, completion: nil)
                
            }
        }
      */
         
        
        
    }

 
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 

}
