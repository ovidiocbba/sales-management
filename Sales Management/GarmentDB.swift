//
//  GarmentDB.swift
//  Sales Management
//
//  Created by Simon Miranda Chiri on 8/20/16.
//  Copyright © 2016 Miranda. All rights reserved.
//

import Foundation
import RealmSwift

class GarmentDB: Object {
    dynamic var id = ""
    dynamic var nameGarment = ""
    dynamic var costPrice: Double = 0.0
    dynamic var salePrice: Double = 0.0
    dynamic var descriptionGarment = ""
    dynamic var photo: NSData? = nil
 
 
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // Specify properties to ignore (Realm won't persist these)
    
    //  override static func ignoredProperties() -> [String] {
    //    return []
    //  }
}
