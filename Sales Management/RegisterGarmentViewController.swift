//
//  RegisterGarmentViewController.swift
//  Sales Management
//
//  Created by Simon Miranda Chiri on 8/15/16.
//  Copyright © 2016 Miranda. All rights reserved.
//

import UIKit
import RealmSwift

class RegisterGarmentViewController: UIViewController, UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{

    @IBOutlet weak var nameModelTextField: UITextField!
    @IBOutlet weak var costPriceTextField: UITextField!
    @IBOutlet weak var salePriceTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var photoImageView: UIImageView!

    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBOutlet weak var cancel: UIBarButtonItem!
    /*
     This value is either passed by `GarmentTableViewController` in `prepareForSegue(_:sender:)`
     or constructed as part of adding a new meal.
     */
    var garment: Garment?
    
    var realm : Realm?
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        realm = try! Realm()
        //print (" the path real is \(realm?.path)")
       // print(Realm.Configuration.defaultConfiguration.path!)
        print(Realm.Configuration.defaultConfiguration.fileURL!)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Handle the text field’s user input through delegate callbacks.
        nameModelTextField.delegate = self
        
        // Set up views if editing an existing Meal.
        if let garment = garment {
            navigationItem.title = garment.nameGarment
            nameModelTextField.text   = garment.nameGarment
            costPriceTextField.text = String(garment.costPrice)
            salePriceTextField.text = String (garment.salePrice)
            descriptionTextField.text = garment.description
            photoImageView.image = garment.photo
            
        }
        
        // Enable the Save button only if the text field has a valid Meal name.
        checkValidMealName()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(sender: UIBarButtonItem) {
        //dismissViewControllerAnimated(true, completion: nil)
       // Depending on style of presentation (modal or push presentation), this view controller needs to be dismissed in two different ways.
        let isPresentingInAddMealMode = presentingViewController is UINavigationController
        
        if isPresentingInAddMealMode {
            dismissViewControllerAnimated(true, completion: nil)
        } else {
            navigationController!.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func selectImageFromPhotoLibrary(sender: UITapGestureRecognizer) {
        //Hide the keyboard
        nameModelTextField.resignFirstResponder()
        
        //UIImagePickerConteroller is a view controller that lets a user pick media from their photo library
        let imagePickerController = UIImagePickerController()
        
        //Only allow photos to be picked, not taken.
        imagePickerController.sourceType = .PhotoLibrary
        
        //Make sure ViewController is notified when the user picks an image.
        imagePickerController.delegate = self
        
        presentViewController(imagePickerController, animated: true, completion: nil)
        
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        //Dismis the picker if the user canceled
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        //The info dictinary contains multiple representation of the image, and this uses the original
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        //Set photoImageView to display the selected image.
        photoImageView.image = selectedImage
        //Dismiss the picker
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    //Validaciones
    // MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        checkValidMealName()
        navigationItem.title = textField.text
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        // Disable the Save button while editing.
        saveButton.enabled = false
    }
    
    func checkValidMealName() {
        // Disable the Save button if the text field is empty.
        let text = nameModelTextField.text ?? ""
        saveButton.enabled = !text.isEmpty
    }
    
    // This method lets you configure a view controller before it's presented.
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
       
        if saveButton === sender {
            let nameGarment = nameModelTextField.text  ?? ""
            let costPrice = Double(costPriceTextField.text!)
            let salePrice = Double(salePriceTextField.text!)
            let description = descriptionTextField.text ?? ""
            let photo = photoImageView.image

            // Set the garment to be passed to GarmentListTableViewController after the unwind segue.
            garment =  Garment(nameGarment: nameGarment,costPrice: costPrice!, salePrice: salePrice!, description: description,photo:  photo);
            let newGarment = GarmentDB()
            newGarment.nameGarment = (garment?.nameGarment)!
            newGarment.costPrice = (garment?.costPrice)!
            newGarment.salePrice = (garment?.salePrice)!
            newGarment.descriptionGarment = (garment?.description)!
            newGarment.photo = UIImagePNGRepresentation((garment?.photo)!)
            newGarment.id = NSUUID().UUIDString
            // save new garment
            try! self.realm?.write {
                self.realm?.add(newGarment)
            }

        }
        
 
        
        
        
         
    }
    
 
    
    
}
