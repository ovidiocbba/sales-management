//
//  RegisterPageViewController.swift
//  Sales Management
//
//  Created by Simon Miranda Chiri on 8/9/16.
//  Copyright © 2016 Miranda. All rights reserved.
//

import UIKit

class RegisterPageViewController: UIViewController {

    @IBOutlet weak var userEmailTextField: UITextField!
    @IBOutlet weak var userPasswordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func registerButtonTapped(sender: AnyObject) {
        
     
        let userEmail = userEmailTextField.text!;
        let userPasssword = userPasswordTextField.text!;
        let userRepeatPassword = repeatPasswordTextField.text!;
        
        //Check for empty fields
        if(userEmail.isEmpty || userPasssword.isEmpty || userRepeatPassword.isEmpty)
        {
            displayMyAlertMessage("All fields are required");
            
            return;
      
        }
       
        //Check if passwords match
        if(userPasssword != userRepeatPassword)
        {
            displayMyAlertMessage("Password do not match");
            
         return;
        }
        
        /*Send user data to server side*/
        let myUrl = NSURL(string:"http://simonmiranda.esy.es/userRegister.php")!;
        let request = NSMutableURLRequest(URL:myUrl);
        
        request.HTTPMethod = "POST";
        let postString = "email=\(userEmail)&password=\(userPasssword)";
        
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding);
        
        let task =  NSURLSession.sharedSession().dataTaskWithRequest(request){
          data,response, error in
          if error != nil
          {
            print("error=\(error)")
            return
          }
            
            print("response = \(response)")
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString!)")
            
            do {
        
                
                let parseJSON = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String: AnyObject]
                // use anyObj here
                let resultValue = parseJSON["status"] as? String;
                
                print("result:\(resultValue)")
                
                var isUserRegistered:Bool = false;
                if(resultValue=="Success"){ isUserRegistered = true;}
                
                var messageToDisplay = parseJSON["message"] as? String;
                if(!isUserRegistered)
                {
                    messageToDisplay = parseJSON["message"] as? String;
                    
                }
                dispatch_async(dispatch_get_main_queue(), {
                    //Display alert message with confirmation
                    var myAlert = UIAlertController(title:"Alert",message:messageToDisplay, preferredStyle: UIAlertControllerStyle.Alert);
                    
                    let okAction = UIAlertAction(title:"Ok",style: UIAlertActionStyle.Default){
                        action in
                        self.dismissViewControllerAnimated(true, completion: nil);
                    }
                    myAlert.addAction(okAction);
                    self.presentViewController(myAlert,animated: true,completion: nil);
                    
                    
                });
                
                
                
            } catch let error as NSError {
                print("json error: \(error.localizedDescription)")
            }
            
          
            
            
        }
        task.resume();
        
        
        
        /*
         //Forma sin Servicios
        
        //Store date
        NSUserDefaults.standardUserDefaults().setObject(userEmail,forKey:"userEmail")
        print(userEmail)
        NSUserDefaults.standardUserDefaults().setObject(userPasssword,forKey:"userPassword")
        print(userPasssword)
        NSUserDefaults.standardUserDefaults().synchronize();
        
        
        //Display alert message with confirmation
        var myAlert = UIAlertController(title:"Alert",message:"Registration is sucessfull, Thanks", preferredStyle: UIAlertControllerStyle.Alert);
        
        let okAction = UIAlertAction(title:"Ok",style: UIAlertActionStyle.Default){
        action in
            self.dismissViewControllerAnimated(true, completion: nil);
        }
        myAlert.addAction(okAction);
        self.presentViewController(myAlert,animated: true,completion: nil);
        */
        
    }
    
    func post()
    {
        let request = NSMutableURLRequest(URL: NSURL(string: "http://www.thebeerstore.ca")!)
        request.HTTPMethod = "POST"
        let postString = "experiment"
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            if error != nil {
                print("error=\(error)")
                return
            }
            
            print("response = \(response)")
            
            let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString!)")
            
        }
        task.resume()
    }
    func displayMyAlertMessage(userMessage:String)
    {
        var myAlert = UIAlertController(title:"Alert",message:userMessage, preferredStyle: UIAlertControllerStyle.Alert);
        
        let okAction = UIAlertAction(title:"Ok",style: UIAlertActionStyle.Default, handler: nil);
        
        myAlert.addAction(okAction);
        
        self.presentViewController(myAlert, animated: true, completion: nil)
        
    }

    @IBAction func iHaveAnAccountButtonTapped(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
