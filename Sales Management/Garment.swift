//
//  Garment.swift
//  Sales Management
//
//  Created by Simon Miranda Chiri on 8/15/16.
//  Copyright © 2016 Miranda. All rights reserved.
//

import UIKit

class Garment{

    //MARK: Properties
    
    var nameGarment: String
    var costPrice: Double
    var salePrice: Double
    var description:String
    var photo: UIImage?
    
    
    init?(nameGarment:String,costPrice: Double, salePrice: Double,description: String,photo: UIImage?)
    {
        //Initialize stored properties.
        self.nameGarment = nameGarment
        self.costPrice = costPrice
        self.salePrice = salePrice
        self.description = description
        self.photo = photo
        
        
        
    }
    
    
    
    
    
    //var photo:
}
